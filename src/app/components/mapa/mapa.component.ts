import { Component, OnInit } from '@angular/core';

// ************************************* LIBRARIES ************************************** //

import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

// ************************************ CLASSES **************************************** //

import { Marcador } from '../../classes/marcador.class';

// ************************************* COMPONENTS ************************************** //

import { MapaEditarComponent } from './mapa-editar.component';

@Component({
  selector: 'app-mapa',
  templateUrl: './mapa.component.html',
  styleUrls: ['./mapa.component.css']
})
export class MapaComponent implements OnInit {

  marcadores: Marcador[] = [];

  lat: number = 51.678418;
  lng: number = 7.809007;

  constructor(
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  )
  {

    if ( localStorage.getItem('marcadores') ){

      this.marcadores = JSON.parse(localStorage.getItem('marcadores'));

    }

  }

  ngOnInit(): void {
  }

  agregarMarcador( evento ): void {

    const { coords } = evento;

    console.log( coords.lat, coords.lng );

    const nuevoMarcador  = new Marcador( coords.lat, coords.lng );

    this.marcadores.push( nuevoMarcador );

    this.guardarStorage();

    // Simple message with an action.
    this.snackBar.open('Marcador agregado', 'Cerrar', { duration: 3000 } );

  }

  guardarStorage(): void {

    localStorage.setItem( 'marcadores', JSON.stringify(this.marcadores) );

  }

  borrarMarcador( i: number ): void{

    this.marcadores.splice(i, 1);

    this.guardarStorage();

    this.snackBar.open('Marcador eliminado', 'Cerrar', { duration: 3000 } );

  }

  editarMarcador( marcador: Marcador ): void {

    const dialogRef = this.dialog.open( MapaEditarComponent , {
      width: '250px',
      data: { titulo: marcador.titulo, desc: marcador.desc }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');

      if ( !result ) {

        return;

      }

      marcador.titulo = result.titulo;
      marcador.desc = result.desc;

      this.guardarStorage();

      this.snackBar.open('Información Actualizada Correctamente!', 'Cerrar', { duration: 1800 } );

    });

  }

}
