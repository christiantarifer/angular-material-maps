import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

// *********************************************** EXTERNAL PACKAGES **************************************** //

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AgmCoreModule } from '@agm/core';

// ********************************************* MODULES **************************************************** //

import { MaterialModule } from './material.module';

// ********************************************* COMPONENTS **************************************************** //

import { MapaComponent } from './components/mapa/mapa.component';
import { MapaEditarComponent } from './components/mapa/mapa-editar.component';



@NgModule({
  entryComponents: [
    MapaEditarComponent
  ],
  declarations: [
    AppComponent,
    MapaComponent,
    MapaEditarComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDjTR3gUFgM1qGJL2A13GqaI8Cj1YHqonE'
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
